enum result{
    FAIL_MALLOC,
    FAIL_HEAP_INIT,
    FAIL_FREE,
    FAIL_HEADER,
    SUCCESS,
};

void get_all_tests(void);