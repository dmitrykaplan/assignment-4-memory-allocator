#include <linux/mman.h>

#include "mem.h"
#include "mem_internals.h"
#include "test.h"

#define HEAP_SIZE 4096
#define BLOCK_SIZE 256


static enum result test1(void){
    void* heap = heap_init(HEAP_SIZE);
    void* block = _malloc(BLOCK_SIZE);
    struct block_header *header = block_get_header(block);

    if(!heap){
        return FAIL_HEAP_INIT;
    }
    if(!block || header->contents != block || header->is_free){
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }
    _free(block);
    munmap(heap, SIZE_MAX);
    return SUCCESS;
}
static enum result test2(void) {
    void *heap = heap_init(HEAP_SIZE);

    if (!heap) {
        return FAIL_HEAP_INIT;
    }

    void *block1 = _malloc(BLOCK_SIZE);
    if (!block1) {
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    void *block2 = _malloc(BLOCK_SIZE);
    if (!block2) {
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    _free(block1);
    if (!block_get_header(block1)->is_free ||
        (block_get_header(block_after(block_get_header(block1))) != block_get_header(block2))) {
        _free(block2);
        munmap(heap, SIZE_MAX);
        return FAIL_FREE;
    }

    _free(block2);
    munmap(heap, SIZE_MAX);
    return SUCCESS;
}

static enum result test3(void) {
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        return FAIL_HEAP_INIT;
    }

    void *block1 = _malloc(BLOCK_SIZE);
    if (!block1) {
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    void *block2 = _malloc(BLOCK_SIZE);
    if (!block2) {
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    void *block3 = _malloc(BLOCK_SIZE);
    if (!block3) {
        _free(block1);
        _free(block2);
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    struct block_header *header1 = block_get_header(block1);
    struct block_header *header3 = block_get_header(block3);

    _free(block2);
    _free(block1);
    if (!header1->is_free || header1->next != header3 || header1->next->contents != block3) {
        _free(block3);
        munmap(heap, SIZE_MAX);
        return FAIL_FREE;
    }
    _free(block3);
    munmap(heap, SIZE_MAX);
    return SUCCESS;
}

static enum result test4(void) {
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        return FAIL_HEAP_INIT;
    }

    void *block1 = _malloc(HEAP_SIZE);
    if (!block1) {
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    struct block_header *header1 = block_get_header(block1);

    if (header1->capacity.bytes != HEAP_SIZE) {
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_HEADER;
    }

    void *block2 = _malloc(HEAP_SIZE);
    if (!block2) {
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    struct block_header *header2 = block_get_header(block2);
    if (header2 != (struct block_header *)(header1->contents + HEAP_SIZE) ||
        header1->next != header2) {
        _free(block2);
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_HEADER;
    }

    _free(block2);
    _free(block1);
    munmap(heap, SIZE_MAX);
    return SUCCESS;
}

static enum result test5(void) {
    void *heap = heap_init(HEAP_SIZE);
    if (!heap) {
        return FAIL_HEAP_INIT;
    }

    void *block1 = _malloc(1);
    if (!block1) {
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }
    struct block_header *header1 = block_get_header(block1);

    void *padding = mmap(
            block_after(header1->next),2 * HEAP_SIZE, PROT_READ | PROT_WRITE,
                 MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0
                 );

    if (padding == MAP_FAILED ||
        padding != header1->next->contents + header1->next->capacity.bytes) {
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_HEADER;
    }

    void *block2 = _malloc(10 * HEAP_SIZE);
    if (!block2) {
        _free(block1);
        munmap(heap, SIZE_MAX);
        return FAIL_MALLOC;
    }

    struct block_header *header2 = block_get_header(block2);

    if (header2 == (void *)(header1->next->contents + header1->next->capacity.bytes) ||
        header1->next->next == (void *)(header1->next->contents + header1->next->capacity.bytes) ||
        header2 != header1->next->next) {
        _free(block1);
        _free(block2);
        munmap(heap, SIZE_MAX);
        return FAIL_HEADER;
    }

    _free(block1);
    _free(block2);
    munmap(padding, SIZE_MAX);
    munmap(heap, SIZE_MAX);
    return SUCCESS;
}

static void get_result( enum result res){
    switch (res) {
        case FAIL_MALLOC:
            puts("Ошибка malloc\n");
            break;
        case FAIL_HEADER:
            puts("Ошибка хедера\n");
            break;
        case FAIL_HEAP_INIT:
            puts("Ошибка создания кучи\n");
            break;
        case FAIL_FREE:
            puts("Ошибка освобождения памяти\n");
            break;
        case SUCCESS:
            puts("Тест прошел успешно\n");
            break;
    }
}

void get_all_tests(void){
    get_result(test1());
    get_result(test2());
    get_result(test3());
    get_result(test4());
    get_result(test5());
}